# Class Assignment 1 Report

## 1. Analysis, Design and Implementation

###1.1. Creating and adjusting your repository
To start this project you need to download on your computer the Tutorial React.js and Spring Data Rest application on Github. 
After, you can create a repository on Bitbucket, for example, and clone this for your computer.
    
    1.Go to command prompt and choose the folder that you want the repository stay using "cd folderName"
    2.Make the clone - paste the path given by your repository, something like "git clone https://...." 

Then, you need to copy the Tutorial(the basic part) to your repository folder.  
Keep the .git file in your repository folder, do not overwrite it, as it contains important settings that will make the connection between your local file and your Bitbucket repository.

To finish this part, you need to do the commit using:

    3.Add the modification to the stage: git add . 
    4.Do the commit: git commit -a -m "first commit"
    5.Push: git push 

###1.2. Create a tag 

To mark the initial state of the repository, it is necessary to create a tag. For this use:
    
    git tag -a v1.2.0 -m "initial version"
    git push origin v1.2.0
 
###1.3. Email-field branch

**Creating this branch:**
    
    git branch email-field

**Add support for email field:**

In the Employee class, add the String email as a new attribute. Make adjustments to the constructor, add get and sets and also make adjustments to the overrides Equals, hashCode and toString methods.

After that, go to the app.js file and add the line Email in the render () of the class EmployeeList extends React.Component to the table, and then also add the line in the render () of the Employee extends React.Component.
In the bundle.js file add "/*#__PURE__*/React.createElement("th ", null," Email ")" on line 34182 and add "/*#__PURE__*/React.createElement("td", null, this.props .employee.email)" on line 34205.
Finally, in the DatabaseLoader class, add the email field and do a commit.

    git checkout -b email-field
    git add .
    git commit -a -m "email-field"
    git push -u origin email-field

**Add unit tests**

Before creating tests, create a method to validate the attributes of Employee. 
Use the constructor to call the method, so the Employee will only be created if the attributes are valid.

**Debugging the server and client parts**

To debug use your IDE, first click on the line where you want to analyze and then click on 'Employee Test' debug, for example. To pass line by line use the F7 key.

To debug on the client side, you can use React developer tools which is an extension of the chrome.

**Merge with master**

To merge with master use:
    
    git checkout master
    git merge email-field

Verify if you have something on stage using
    
    git log

Then, if it's necessary do the commit and push. Finally, add the second tag.

    git tag -a v1.3.0 -m "second version"
    git push origin v1.3.0
    git tag -> to see all tags

###1.4.Fix-invalid-email

Create a new branch to validate the email using:

    git branch fix-invalid-email
    git checkout fix-invalid-email

Debug the application as the previous steps on 1.3. And then, create a new tag.

    git tag -a v1.3.1 -m "1.3.1 version"
    git push origin v1.3.1 

Finally, do the checkout to master and merge with fix-invalid-email.
    
    git checkout master
    git merge fix-invalid-email
    git push

Assign your repository with the last tag ca1.

    git tag ca1
    git push origin ca1

## 2. Analysis of an Alternative

An alternative to Git is Mercurial. It is widely used, mainly by large companies like Facebook, Mozilla and W3. As GIT, Mercurial is effective in recording the evolution of the project, enabling teamwork and creating and maintaining variations of the project.

Regarding performance, both have practically the same performance and the differences are not very large.

Both have distributed version control, so they are efficient for cases where there is a project with hundreds of developers, geographically distributed teams, external collaborations or complex workflows.

One of the most prominent differences between Git and Mercurial is the level of knowledge required to use the systems. In short, Git is more complex and requires you and your team to get to know it inside and out before you can expect to use it effectively and safely.

Git’s branching model is more effective than Mercurial. While Git allows you to create, delete, and change a branch anytime, without affecting the commits, the Mercurial embeds the branches in the commits, where they are stored forever. This means that branches cannot be removed because that would alter the history.

Both Git and Mercurial are good, they were just designed differently and require different levels of expertise.
As Git has become an industry standard, more developers are familiar it. But the choice between one and the other will depend on the type of application, whether it is desktop or mobile, whether your team is very large and global or small, or whether your security requirements are very important.

*From: https://www.perforce.com/blog/vcs/git-vs-mercurial-how-are-they-different*

## 3. Implementation of the Alternative

###1.3.1. Starting

Using Mercurial is very similar to Git. First, you need to download it to your computer and create a new repository. 
Then, you can find a project hosting service, in which case I used Helix TeamHub which is very similar to Bitbucket.

The steps to follow are the same, first we make a clone:

    hg clone http:...repositoryname

After that you can pass the Tut-basic to the repository file. 

Using comand line, do the first commit:

    hg commit -m "first commit"

The "add"(like git add) is not necessary in Mercurial. Then do push:

    hg push

###1.3.2. Creating a tag

To create a tag in Mercurial:

    hg tag v1.2.0 -m "first tag"
    hg push

You can mark solved your issues putting into the message "#issuenumber".

###1.3.3. Email-field branch 

To create a branch:

    hg branch email-field
    hg update --> to start work into the branch

Make the necessary changes then commit:

    hg commit -m "email-field"
    hg push --new-branch (for the first time)

To return to your "master", which in the case of Mercurial is called Default:

    hg update default

Then merge and push:

    hg merge email-field
    hg push

Don't forget to add the second tag:

    hg tag v1.3.0 -m "second tag"
    hg push

###1.3.4. Fix-invalid-email

The process for adding a branch is the same as the previous one. Then you can follow these commands.

    hg branch fix-invalid-email
    hg update fix-invalid-email

    ----- make changes: add methods, tests.. ----
    hg commit -m "fix-invalid-email"
    hg push --new-branch

    ----merge with Default ---
    hg update default
    hg merge fix-invalid-email

    ---add tag ---
    hg tag v1.3.1 -m "task 3"
    hg push

    ----When finish the exercice----
    hg tag ca1 -m "ca1 completed using mercurial"
    hg push

