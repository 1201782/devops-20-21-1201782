# Class Assignment 2 Report - Part 1

## 1. Implementation

In this first part of this assignment we use gradle to compile a demo application for a chat room.
To start part 1 of this assignment it is necessary to:
- Install gradle 6.6
- Create a folder called 'part1' inside the CA2 folder of your repository;
- Download and commit the application:
  https://bitbucket.org/luisnogueira/gradle_basic_demo/ and insert it in the part1 folder;
- On bitbucket, create issues.

Then read the instructions in the README.md file. There are commands that must be executed on the command line.

###Build

    ./gradlew build - to build the project

In order to build successfully I needed to use Java SDK 8 and in the file in part1/gradle/wrapper/gradle-wrapper.properties I needed to change the 'distibutionUrl' to *distributionUrl=https://services.gradle.org/distributions/gradle-6.3-bin.zip*

###Execute the server

To execute the server part, it is necessary to **create a task** in the file build.gradle. This task consists of copying the task that runs the client and making some changes, as description and main:

    task runServer (type: JavaExec, dependsOn: classes) {
        group = "DevOps"

        description = "Launches a chat server that connects to a client on localhost: 59001"

        classpath = sourceSets.main.runtimeClasspath

        main = 'basic_demo.ChatServerApp'

        args 'localhost', '59001'
    }

To execute it, just type in the command line:

    java -cp build / libs / basic_demo-0.1.0.jar basic_demo.ChatServerApp <server port> - (replace <server port> by 59001) 

###Execute the client

After running the server, run the client part through the command line, then just open other windows, execute the client again and start the chat.

    ./gradlew runClient 

###Add a unit Test

Inside the src, create another folder called test and make the association as a test in the project structure. 
Then add the lines below and go to build.gradle and in dependencies add *implementation 'junit: junit: 4.12'*.

    package basic_demo;
    import org.junit.Test;
    import static org.junit.Assert.*;
    public class AppTest {
        @Test public void testAppHasAGreeting() {
            App classUnderTest = new App();
            assertNotNull("app should have a greeting", classUnderTest.getGreeting());
    }
    }

To run the test type on the command line:

    ./gradlew test

###Create a backup folder

This task has the objective to make a backup of the application's source folder. 
To perform this task, it is necessary to create a new task of the Copy type, indicating what you want to copy and wherever you want to go and then execute it.

    task copyFiles(type: Copy){
    from 'src/main/java/basic_demo'
    into 'backup'
    }

To execute just type on the command line:

    ./gradle build copyFiles

###Add a zip file

The purpose of this task is to maque an archive of the sources of the application. It should copy the contents of the src folder to a new zip file.
To do this it is necessary to create a new task in the build.gradle file of Zip type and indicate the name of the .zip file, the source and the destination.

    task zipFile(type: Zip){
    archiveFileName = 'src.zip'
    destinationDirectory = file('zip')
    from ('src')
    }

To execute just type on the command line:

    ./gradle build zipFile

Finally, make the last commit and add the tag ca2-part1.



