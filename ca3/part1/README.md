# Class Assignment 3 - part1 Report

To fulfill the first part of this task about virtualization with Vagrant it was necessary to:

- Download and install Oracle VM Virtualbox;
- Download the Linux CD through the link: https://help.ubuntu.com/community/Installation/MinimalCD;
  
In my case, for the virtual machine to work, I had to go to the advanced BIOS settings of my computer and give permission to use virtualization there;

- At Virtualbox, I created a new host-only network adapter
- Start the VM and install Ubuntu(before finishing, remove the linux CD);
- In the VM:
    - Update the packages repositories:      
    
            sudo apt update**   
    
- Install the network tools: 
  
        sudo apt install net-tools**  
  
- Edit the network configuration file to set up the IP:
  
        sudo nano /etc/netplan/01-netcfg.yaml 
  
- Insert the following code snippet (with the same tabs)

        network:
            version: 2
            renderer: networkd
            ethernets:
                enp0s3:
                    dhcp4: yes
                enp0s8:
                    addresses:
                        - 192.168.56.5/24

  - Apply the new changes:  
    
        sudo netplan apply
    
- Next, it was necessary to install opnenssh-server so that we could access the virtual machine through the host (our physical computer)
  
       sudo apt install openssh-server
- It's necessary to enable password authentication for ssh:
  
        sudo nano /etc/ssh/sshd_config
  
  - Uncomment the line PasswordAuthentication yes; 
    
        sudo service ssh restart
    
- To transfers files to / from the VM (from other hosts) it's necessary install a ftp server:
  
        sudo apt install vsftpd
  
- Finally, enable write access for vsftpd:
  
        sudo nano /etc/vsftpd.conf
  
  - Uncomment the line write_enable = YES
  
        sudo service vsftpd restart
    
- On the physical machine, it was necessary to open the terminal and write:
  
        ssh username@ipOfVM (e.g. ssh thayane@192.168.56.5)
  
- Since the FTP server is enabled I could use the ftp application, then we download FileZilla, it's used to transfer files to or from the VM.
- Using the terminal (host) install Git and JDK 8:
  
        sudo apt install git
  
        sudo apt install the penjdk-8-jdk-headless

Now, we need to clone the tut-react repository that we use as the basis for our projects. I did it using the terminal of my physical machine through ssh.
After cloning, change the directory to the basic folder and run the application:
    
    ./mvnw spring-boot: run

If you go to the browser and put the url: http://192.168.56.5:8080 the content of the application frontend will appear.

After running this application successfully, you also need to run the application made with Gradle (from the previous assignments).

I started with part2 and for that, I went to FileZilla and copied the demo folder, which is saved on my computer and pasted it in the folder called thayane inside my virtual machine. 
And here was my mistake, after that I was not able to build the application in any way. So I started again and cloned my entire repository to the virtual machine, then gave permission to run the gradlew and did build and then run:

    ./gradlew build
    ./gradlew tasks
    ./gradlew bootRun

And so the application worked, and the frontend appeared in the browser.

For part1 of CA2, I was able to build and run the server through the VM. To execute the client part I created a new task:
  
    task runClientVM(type:JavaExec, dependsOn: classes){
    group = "DevOps"
    description = "Launches a chat client that connects to a server on http://192.168.56.5:59001/ "

    classpath = sourceSets.main.runtimeClasspath

    main = 'basic_demo.ChatClientApp'

    args '192.168.56.5', '59001'
    }

Then I run the client side through ssh using my cmd. Finally, it worked.