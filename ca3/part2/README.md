# Class Assignment 3 - part2 Report

## 1. Implementation

In the second part of this work, we created two virtual machines using Vagrant, one VM is *web*, used to run tomcat and spring boot (basic application) and another is *db*, responsible for running the H2 server database.
To create these virtual machines, we use the repository: https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/src/master/
First, it is necessary to copy and paste the Vagrantfile file into the local computer inside the folder where the project will be inserted. Also copy the tut-basic-gradle project to this folder.
The following are some changes to this file:

1. On line 70, change the link to your repository link;
   
2. On line 71, write the path to your project (eg: devops-20-21-1201782/ca3/part2/ass1);

3. In line 75, replace, if necessary, the name of the file to be copied ./build/libs/demo-0.0.1-SNAPSHOT.war, in my case, I had to replace "basic" with "demo";

4. Next, it is necessary to open this repository and make changes to the project https://bitbucket.org/atb/tut-basic-gradle/src. Through commits it is possible to see which ones are needed. It is important to pay attention to the content, as the "demo-0.0.1-SNAPSHOT" file will be cited in some places as "basic-0.0.1-SNAPSHOT", in my case I needed to change this as well.

5. Check the java version on build.gradle file, it was necessary to change to java '1.8' in my case.

After that, you need to use the command line, go to the folder where we have the Vagrantfile and run Vagrant using the **vagrant up** command.

If all goes well, the machines will be created in your Virtualbox. Next, to check if the application worked, go to the browser and type one of the links:

    http: //localhost:8080/demo-0.0.1-SNAPSHOT/
    http://192.168.33.10:8080/demo-0.0.1-SNAPSHOT/

To open H2, just go to one of the links below (use this url to access- jdbc: h2: tcp: //192.168.33.11: 9092 /./ jpadb)

    http: //localhost:8080/demo-0.0.1-SNAPSHOT/h2-console
    http://192.168.33.10:8080/demo-0.0.1-SNAPSHOT/h2-console

In the browser you will see something like this image:

![](./demo/images/web.png)

Using H2 you will see something like the image below:

![](./demo/images/H2.png)

## 2. Technological alternative for virtualization tool

### 2.1 Analysis

In this session we will see an alternative to Virtualbox and how this tool can solve the same objectives as the previous task.
The alternative tool of Virtualbox that I choose was **Hyper-V**. Microsoft Hyper-V, also known as Viridian and formerly Virtualization on Windows Server, is a virtualization technology based on native Hypervisor.

Hyper-V was launched for the first time with Windows Server 2008 and is available at no additional cost for all Windows Server and Windows 8 and later. A stand-alone Windows Hyper-V Server is free, but only with a command line interface. 

Hyper-V is available on 64-bit versions of Windows 10 Pro, Enterprise, and Education. It is not available on the Home edition. 

Most computers run Hyper-V, however each virtual machine runs a completely separate operating system. You can generally run one or more virtual machines on a computer with 4GB of RAM, though you'll need more resources for additional virtual machines or to install and run resource intense software like games, video editing, or engineering design software.

Hyper-V on Windows supports many operating systems in a virtual machine including various releases of Linux, FreeBSD, and Windows.

Programs that depend on specific hardware will not work well in a virtual machine. For example, games or applications that require processing with GPUs might not work well. Also, applications relying on sub-10ms timers such as live music mixing applications or high precision times could have issues running in a virtual machine.

Hyper-V has some limitations,for example, if you have Hyper-V enabled, those latency-sensitive, high-precision applications may also have issues running in the host. This is because with virtualization enabled, the host OS also runs on top of the Hyper-V virtualization layer, just as guest operating systems do.
However, unlike guests, the host OS is special in that it has direct access to all the hardware, which means that applications with special hardware requirements can still run without issues in the host OS.

Now, let's make a comparison between Virtualbox and Hyper-V using pros and cons.


|  Virtualbox    | 
|:-----------:|
| ✔ You can activate multiple VMs on a host-only network.     |           
| ✔ Cross-platform functionality allows you to run it on Linux Desktop and can be accessed on a Windows or Mac OSX system. |   
| ✔ VirtualBox seems to have the most support and documentation when it comes to compatibility with Vagrant. | 
| ❌ The memory footprint is high and requires more than 8 GB of memory to run an Ubuntu VM without any problems. | 
| ❌ Some known issues with OVF/OVA images import/export. |  
| ❌ Some intermittent issues when mounting external hard drives and trying to access them from within the running VM.  | 


|  Hyper-V    | 
|:-----------:|
| ✔  Allows you to dynamically add storage, memory and network, all without turning off the VM.   |           
| ✔  You can set the VM's resources to dynamically adjust in response to what other VMs are doing. |   
| ✔  The Replica feature means it's not necessary invest in expensive and complicated clustering technology if your goal is simply to achieve resiliency against hardware failure. |
| ✔  Being Windows SysAdmin, Hyper-V is intuitive to deploy, monitor, and manage.|
| ❌  Checkpoints are snapshots of the system, where you can revert the system to an earlier point in time if you encounter problems after installing updates, applications, or making changes to the system. Versions prior to Server 2016 create all kinds of problems if a VM needs to be recovered.| 
| ❌  Some people find the support for Linux weak.|  
| ❌  Requires all VMs to be taken offline for a reboot during routine security updates to OS. |  


### 2.2 Implementation 

To install the Hyper-V I follow the tutorial on https://docs.microsoft.com/en-us/virtualization/hyper-v-on-windows/quick-start/enable-hyper-v.

After downloading Hyper-v, you need to enable it on your operating system. Just go to Windows >> Settings >> Applications >> Programs and Features >> Enable or disable Windows features >> and enable hyper-v.

After that I created a new folder called part2b, where I cloned the demo application and also the Vagrantfile file.
In the Vagrantfile file it is necessary to change a few lines:

*1-* In the 4th line, add this information:

    config.vm.provider "hyperv"

*2-* On lines 5, 20, and 49 change the box name to

**"generic / ubuntu1604"**

*3-* In line 71 check if the path to the file is correct

Then do the git commit and go to the command line and run **vagrant up**. Here it's important to know that you are able to run this as administrator only.

After making the build successfully, it is necessary to change the ip, this is because hyper-v generates and associates an ip when creating its virtual machines, it does not assume what we put in the vagrantfile file, which is a disadvantage, since it is necessary to do this manually.
To know the ip that has been assigned, just go to the Hyper-v Manager, click on the web machine, in the menu at the bottom go to the network.
Then, access the virtual machine 'web' through ssh, and go to src/main/resources/application.properties and change the ip, on the third line, with the ip generated by hyper-v. Exit the machine and then do:

    vagrant reload --provision


![](./demo/images/build-hyperv.png)

In this way, Vagrant updates the information entered and makes a new build. With this part completed successfully it is possible to go to the browser and access the content of the application through the links:

**web:**
http://192.168.242.43:8080/demo-0.0.1-SNAPSHOT/

![](./demo/images/web-hyperv.png)


**h2:**
http://192.168.242.43.10:8080/demo-0.0.1-SNAPSHOT/h2-console
To acesse h2 the URL is: jdbc:h2:tcp://192.168.250168:9092/./jpadb (It uses the 'db' ip address)

![](./demo/images/h2-hyperv.png)

So I consider this task completed.

*Fonts:*

https://pt.wikipedia.org/wiki/Hyper-V

https://www.trustradius.com/compare-products/hyper-v-vs-oracle-vm-virtualbox

https://docs.microsoft.com/en-us/virtualization/hyper-v-on-windows/about/

https://www.atlantech.net/blog/microsoft-hyper-v-performance-and-cloud-hosting-pros-and-cons