/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.greglturnquist.payroll;

import java.util.Objects;
import java.util.regex.Pattern;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author Greg Turnquist
 */
// tag::code[]
@Entity // <1>
public class Employee {

	private @Id @GeneratedValue Long id; // <2>
	private String firstName;
	private String lastName;
	private String description;
	private String jobTitle;
	private String email;

	private Employee() {}

	public Employee(String firstName, String lastName, String description, String jobTitle, String email) {
		setFirstName(firstName);
		setLastName(lastName);
		setDescription(description);
		setJobTitle(jobTitle);
		setEmail(email);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Employee employee = (Employee) o;
		return Objects.equals(id, employee.id) &&
			Objects.equals(firstName, employee.firstName) &&
			Objects.equals(lastName, employee.lastName) &&
			Objects.equals(description, employee.description) &&
			Objects.equals(email, employee.email);
	}

	@Override
	public int hashCode() {

		return Objects.hash(id, firstName, lastName, description, email);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		if (!validateFirstName(firstName)) {
			throw new IllegalArgumentException("First name cannot be empty or null");
		}
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		if (!validateLastName(lastName)) {
			throw new IllegalArgumentException("Last name cannot be empty or null");
		}
		this.lastName = lastName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		if (!validateDescription(description)) {
			throw new IllegalArgumentException("Description cannot be empty or null");
		}
		this.description = description;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		if (!validateJobTitle(jobTitle)) {
			throw new IllegalArgumentException("Job title cannot be empty or null");
		}
		this.jobTitle = jobTitle;
	}

	public String getEmail() {return email; }

	public void setEmail(String email) {
		if (!validateEmail(email)) {
			throw new IllegalArgumentException("Email cannot be empty or null or have an invalid format");
		}
		this.email = email;
	}

	private boolean validateFirstName(String firstName) {
		return firstName != null && !firstName.equals("");
	}

	private boolean validateLastName(String lastName) {
		return lastName != null && !lastName.equals("");
	}

	private boolean validateDescription(String description) {
		return description != null && !description.equals("");
	}

	private boolean validateJobTitle(String jobTitle) {
		return jobTitle != null && !jobTitle.equals("");
	}

	private boolean validateEmail(String email) {
		return email != null && !email.equals("") && validateEmailFormat(email);
	}

	private boolean validateEmailFormat(String email) {
		// Extracted from https://www.geeksforgeeks.org/check-email-address-valid-not-java/
		String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
				"[a-zA-Z0-9_+&*-]+)*@" +
				"(?:[a-zA-Z0-9-]+\\.)+[a-z" +
				"A-Z]{2,7}$";

		Pattern pat = Pattern.compile(emailRegex);
		return pat.matcher(email).matches();

	}

	@Override
	public String toString() {
		return "Employee{" +
			"id=" + id +
			", firstName='" + firstName + '\'' +
			", lastName='" + lastName + '\'' +
			", description='" + description + '\'' +
			", jobTitle='" + jobTitle + '\'' +
			", email='" + email + '\''	+
			'}';
	}
}

// end::code[]
