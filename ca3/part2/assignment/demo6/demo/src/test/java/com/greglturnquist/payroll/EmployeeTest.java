package com.greglturnquist.payroll;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {
    @Test
    void EmployeeTestConstructor_ValidFirstName() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow.white@disney.com";

        //Act
        Employee employee = new Employee(firstName,lastName,description,jobTitle,email);

        //Assert
        assertNotNull(employee);
        assertEquals(firstName,employee.getFirstName());

    }

    @Test
    void EmployeeTestConstructor_EmptyFirstName() {

        //Arrange
        String firstName = "";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow.white@disney.com";

        //Act+Assert
        assertThrows(IllegalArgumentException.class, () -> { new Employee(firstName,lastName,description,jobTitle,email);});


    }

    @Test
    void EmployeeTestConstructor_NullFirstName() {

        //Arrange
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow.white@disney.com";

        //Act+Assert
        assertThrows(IllegalArgumentException.class, () -> { new Employee(null,lastName,description,jobTitle,email);});


    }

    @Test
    void setEmployeeFirstName_ValidFirstName() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow.white@disney.com";
        Employee employee = new Employee(firstName,lastName,description,jobTitle,email);
        String newFirstName = "Rain";

        //Act
        employee.setFirstName(newFirstName);

        //Assert
        assertNotNull(employee);
        assertEquals(newFirstName,employee.getFirstName());

    }

    @Test
    void setEmployeeFirstName_EmptyFirstName() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow.white@disney.com";
        Employee employee = new Employee(firstName,lastName,description,jobTitle,email);
        String newFirstName = "";

        //Act+Assert
        assertThrows(IllegalArgumentException.class, () -> { employee.setFirstName(newFirstName);});

    }

    @Test
    void setEmployeeFirstName_NullFirstName() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow.white@disney.com";
        Employee employee = new Employee(firstName,lastName,description,jobTitle,email);

        //Act+Assert
        assertThrows(IllegalArgumentException.class, () -> { employee.setFirstName(null);});

    }

    @Test
    void EmployeeTestConstructor_ValidLastName() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow.white@disney.com";

        //Act
        Employee employee = new Employee(firstName,lastName,description,jobTitle,email);

        //Assert
        assertNotNull(employee);
        assertEquals(lastName,employee.getLastName());

    }

    @Test
    void EmployeeTestConstructor_EmptyLastName() {

        //Arrange
        String firstName = "Snow";
        String lastName = "";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow.white@disney.com";

        //Act+Assert
        assertThrows(IllegalArgumentException.class, () -> { new Employee(firstName,lastName,description,jobTitle,email);});


    }

    @Test
    void EmployeeTestConstructor_NullLastName() {

        //Arrange
        String firstName = "Snow";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow.white@disney.com";

        //Act+Assert
        assertThrows(IllegalArgumentException.class, () -> { new Employee(firstName,null,description,jobTitle,email);});


    }

    @Test
    void setEmployeeLastName_ValidLastName() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow.white@disney.com";
        Employee employee = new Employee(firstName,lastName,description,jobTitle,email);
        String newLastName = "Black";

        //Act
        employee.setLastName(newLastName);

        //Assert
        assertNotNull(employee);
        assertEquals(newLastName,employee.getLastName());

    }

    @Test
    void setEmployeeLastName_EmptyLastName() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow.white@disney.com";
        Employee employee = new Employee(firstName,lastName,description,jobTitle,email);
        String newLastName = "";

        //Act+Assert
        assertThrows(IllegalArgumentException.class, () -> { employee.setLastName(newLastName);});

    }

    @Test
    void setEmployeeLastName_NullLastName() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow.white@disney.com";
        Employee employee = new Employee(firstName,lastName,description,jobTitle,email);

        //Act+Assert
        assertThrows(IllegalArgumentException.class, () -> { employee.setLastName(null);});

    }

    @Test
    void EmployeeTestConstructor_ValidDescription() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow.white@disney.com";

        //Act
        Employee employee = new Employee(firstName,lastName,description,jobTitle,email);

        //Assert
        assertNotNull(employee);
        assertEquals(description,employee.getDescription());

    }

    @Test
    void EmployeeTestConstructor_EmptyDescription() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "";
        String jobTitle = "apple taster";
        String email = "snow.white@disney.com";

        //Act+Assert
        assertThrows(IllegalArgumentException.class, () -> { new Employee(firstName,lastName,description,jobTitle,email);});


    }

    @Test
    void EmployeeTestConstructor_NullDescription() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String jobTitle = "apple taster";
        String email = "snow.white@disney.com";

        //Act+Assert
        assertThrows(IllegalArgumentException.class, () -> { new Employee(firstName,lastName,null,jobTitle,email);});


    }

    @Test
    void setEmployeeDescription_ValidDescription() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow.white@disney.com";
        Employee employee = new Employee(firstName,lastName,description,jobTitle,email);
        String newDescription = "character";

        //Act
        employee.setDescription(newDescription);

        //Assert
        assertNotNull(employee);
        assertEquals(newDescription,employee.getDescription());

    }

    @Test
    void setEmployeeDescription_EmptyDescription() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow.white@disney.com";
        Employee employee = new Employee(firstName,lastName,description,jobTitle,email);
        String newDescription = "";

        //Act+Assert
        assertThrows(IllegalArgumentException.class, () -> { employee.setDescription(newDescription);});

    }

    @Test
    void setEmployeeDescription_NullDescription() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow.white@disney.com";
        Employee employee = new Employee(firstName,lastName,description,jobTitle,email);

        //Act+Assert
        assertThrows(IllegalArgumentException.class, () -> { employee.setDescription(null);});

    }

    @Test
    void EmployeeTestConstructor_ValidJobTitle() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow.white@disney.com";

        //Act
        Employee employee = new Employee(firstName,lastName,description,jobTitle,email);

        //Assert
        assertNotNull(employee);
        assertEquals(jobTitle,employee.getJobTitle());

    }

    @Test
    void EmployeeTestConstructor_EmptyJobTitle() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "";
        String email = "snow.white@disney.com";

        //Act+Assert
        assertThrows(IllegalArgumentException.class, () -> { new Employee(firstName,lastName,description,jobTitle,email);});


    }

    @Test
    void EmployeeTestConstructor_NullJobTitle() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String email = "snow.white@disney.com";

        //Act+Assert
        assertThrows(IllegalArgumentException.class, () -> { new Employee(firstName,lastName,description,null,email);});


    }

    @Test
    void setEmployeeJobTitle_ValidJobTitle() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow.white@disney.com";
        Employee employee = new Employee(firstName,lastName,description,jobTitle,email);
        String newJobTitle = "dwarf helper";

        //Act
        employee.setJobTitle(newJobTitle);

        //Assert
        assertNotNull(employee);
        assertEquals(newJobTitle,employee.getJobTitle());

    }

    @Test
    void setEmployeeJobTitle_EmptyJobTitle() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow.white@disney.com";
        Employee employee = new Employee(firstName,lastName,description,jobTitle,email);
        String newJobTitle = "";

        //Act+Assert
        assertThrows(IllegalArgumentException.class, () -> { employee.setJobTitle(newJobTitle);});

    }

    @Test
    void setEmployeeJobTitle_NullJobTitle() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow.white@disney.com";
        Employee employee = new Employee(firstName,lastName,description,jobTitle,email);

        //Act+Assert
        assertThrows(IllegalArgumentException.class, () -> { employee.setJobTitle(null);});

    }

    @Test
    void EmployeeTestConstructor_ValidEmail() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow.white@disney.com";

        //Act
        Employee employee = new Employee(firstName,lastName,description,jobTitle,email);

        //Assert
        assertNotNull(employee);
        assertEquals(email,employee.getEmail());

    }

    @Test
    void EmployeeTestConstructor_EmptyEmail() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "";

        //Act+Assert
        assertThrows(IllegalArgumentException.class, () -> { new Employee(firstName,lastName,description,jobTitle,email);});

    }

    @Test
    void EmployeeTestConstructor_NullEmail() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";

        //Act+Assert
        assertThrows(IllegalArgumentException.class, () -> { new Employee(firstName,lastName,description,jobTitle,null);});

    }

    @Test
    void EmployeeTestConstructor_EmailWithInvalidFormat_PrefixWithInvalidCharacter() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow#white@disney.com";

        //Act+Assert
        assertThrows(IllegalArgumentException.class,() -> {new Employee(firstName,lastName,description,jobTitle,email);});

    }

    @Test
    void EmployeeTestConstructor_EmailWithInvalidFormat_PrefixWithDoubleDot() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow..white@disney.com";

        //Act+Assert
        assertThrows(IllegalArgumentException.class,() -> {new Employee(firstName,lastName,description,jobTitle,email);});

    }

    @Test
    void EmployeeTestConstructor_EmailWithInvalidFormat_MissingAtSymbol() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow.whitedisney.com";

        //Act+Assert
        assertThrows(IllegalArgumentException.class,() -> {new Employee(firstName,lastName,description,jobTitle,email);});

    }

    @Test
    void EmployeeTestConstructor_EmailWithInvalidFormat_DomainWithDoubleDot() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow.white@disney..com";

        //Act+Assert
        assertThrows(IllegalArgumentException.class,() -> {new Employee(firstName,lastName,description,jobTitle,email);});

    }

    @Test
    void EmployeeTestConstructor_EmailWithInvalidFormat_DomainWithMoreThanSevenCharactersAfterDot() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow.white@disney.commmmmm";

        //Act+Assert
        assertThrows(IllegalArgumentException.class,() -> {new Employee(firstName,lastName,description,jobTitle,email);});

    }

    @Test
    void EmployeeTestConstructor_EmailWithInvalidFormat_DomainWithLessThanTwoCharactersAfterDot() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow.white@disney.c";

        //Act+Assert
        assertThrows(IllegalArgumentException.class,() -> {new Employee(firstName,lastName,description,jobTitle,email);});

    }

    @Test
    void EmployeeTestConstructor_EmailWithInvalidFormat_DomainWithInvalidCharacter() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow.white@disney!.com";

        //Act+Assert
        assertThrows(IllegalArgumentException.class,() -> {new Employee(firstName,lastName,description,jobTitle,email);});

    }

    @Test
    void setEmployeeEmail_ValidEmail() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow.white@disney.com";
        Employee employee = new Employee(firstName,lastName,description,jobTitle,email);
        String newEmail = "snow.white_sequel@disney.com";

        //Act
        employee.setEmail(newEmail);

        //Assert
        assertNotNull(employee);
        assertEquals(newEmail,employee.getEmail());

    }

    @Test
    void setEmployeeEmail_EmptyEmail() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow.white@disney.com";
        Employee employee = new Employee(firstName,lastName,description,jobTitle,email);
        String newEmail = "";

        //Act+Assert
        assertThrows(IllegalArgumentException.class, () -> { employee.setEmail(newEmail);});

    }

    @Test
    void setEmployeeEmail_NullEmail() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow.white@disney.com";
        Employee employee = new Employee(firstName,lastName,description,jobTitle,email);

        //Act+Assert
        assertThrows(IllegalArgumentException.class, () -> { employee.setEmail(null);});

    }

    @Test
    void setEmployeeEmail_EmailWithInvalidFormat_PrefixWithInvalidCharacter() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow.white@disney.com";
        Employee employee = new Employee(firstName,lastName,description,jobTitle,email);
        String newEmail = "snow#white@disney.com";

        //Act+Assert
        assertThrows(IllegalArgumentException.class, () -> { employee.setEmail(newEmail);});

    }

    @Test
    void setEmployeeEmail_EmailWithInvalidFormat_PrefixWithDoubleDot() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow.white@disney.com";
        Employee employee = new Employee(firstName,lastName,description,jobTitle,email);
        String newEmail = "snow..white@disney.com";

        //Act+Assert
        assertThrows(IllegalArgumentException.class, () -> { employee.setEmail(newEmail);});

    }

    @Test
    void setEmployeeEmail_EmailWithInvalidFormat_MissingAtSymbol() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow.white@disney.com";
        Employee employee = new Employee(firstName,lastName,description,jobTitle,email);
        String newEmail = "snow.whitedisney.com";

        //Act+Assert
        assertThrows(IllegalArgumentException.class, () -> { employee.setEmail(newEmail);});

    }

    @Test
    void setEmployeeEmail_EmailWithInvalidFormat_DomainWithDoubleDot() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow.white@disney.com";
        Employee employee = new Employee(firstName,lastName,description,jobTitle,email);
        String newEmail = "snow.white@disney..com";

        //Act+Assert
        assertThrows(IllegalArgumentException.class, () -> { employee.setEmail(newEmail);});

    }

    @Test
    void setEmployeeEmail_EmailWithInvalidFormat_DomainWithMoreThanSevenCharactersAfterDot() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow.white@disney.com";
        Employee employee = new Employee(firstName,lastName,description,jobTitle,email);
        String newEmail = "snow.white@disney.commmmmm";

        //Act+Assert
        assertThrows(IllegalArgumentException.class, () -> { employee.setEmail(newEmail);});

    }

    @Test
    void setEmployeeEmail_EmailWithInvalidFormat_DomainWithLessThanTwoCharactersAfterDot() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow.white@disney.com";
        Employee employee = new Employee(firstName,lastName,description,jobTitle,email);
        String newEmail = "snow.white@disney.c";

        //Act+Assert
        assertThrows(IllegalArgumentException.class, () -> { employee.setEmail(newEmail);});

    }


    @Test
    void setEmployeeEmail_EmailWithInvalidFormat_DomainWithInvalidCharacter() {

        //Arrange
        String firstName = "Snow";
        String lastName = "White";
        String description = "princess";
        String jobTitle = "apple taster";
        String email = "snow.white@disney.com";
        Employee employee = new Employee(firstName,lastName,description,jobTitle,email);
        String newEmail = "snow.white@disney!.com";

        //Act+Assert
        assertThrows(IllegalArgumentException.class, () -> { employee.setEmail(newEmail);});

    }


}