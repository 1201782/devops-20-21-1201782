# Class Assignment 2 Report - Part 2

## 1. Analysis, Design and Implementation

### 1.1. Brief introduction

Gradle is a very powerful open source build tool based on Ant and Maven that allows you to configure build files using its DSL (Domain Specific Language) based on the Groovy language. 
Gradle and other build tools, basically, automate all routine tasks of a project in an organized way that prevents the developer from having to waste time.
Gradle's idea is based on tasks, i.e. when we want to create some new behavior during the build, we create a task.

### 1.2. About this project

The project in which we implemented Gradle is a project that uses the Java language (in my case, Java 11), the Springboot Framework, and some dependencies like Rest Repositories, Thymeleaf, JPA, and H2.
To start, we created a new project on the website https://start.spring.io/, where we inserted the settings above and made the download. Then it was necessary to unzip the document into the folder CA2 / part2 that we created for this project.
Now you can build using gradle, write on the command line:

    % ./gradlew build

To check the gradle tasks available you can write:

    % ./gradlew tasks  

### 1.3. Implementation

After placing the project in the ca2 folder of the repository, you need to follow the following steps:

- On git create a new branch called "tut-basic-gradle" and do the checkout;
- Delete the src folder and copy the src folder from tut-basic project;
- Copy the files  webpack.config.js and package.json from tut-basic;
- Delete the folder src/main/resources/static/built/.

To try to run the Springboot application you can use:

    % ./gradlew bootRun

In this part, the build failed to me, but after execute the application through IDE it worked perfectly.

### Frontend part

As expected, the web page http: // localhost: 8080 ill be empty, and then you need to configure the Frontend part.

To do this, go to file **build.gradle**, then, plugins and paste the following line of code:

    id "org.siouan.frontend" version "1.4.1"

At the end of this same file paste:

    frontend {
    nodeVersion = "12.13.1"
    assembleScript = "run webpack"
    }

After that, go to the **package.json** file and replace the script snippet with the one below:

    "scripts": {
    "watch": "webpack --watch -d",
    "webpack": "webpack"
    },

Then make the build to execute the frontend together with the application and then execute.

    % ./gradlew build 
    % ./gradlew bootRun

Note that after running, if you go to localhost:8080 the page content will already appear.

### A task to copy a file

The next requirement is to copy a jar file located in the build/libs folder and create another folder called "dist" to keep that copy. For this:

#### Create the task:

    task copyJarFile(type: Copy){
    from 'build/libs/demo-0.0.1-SNAPSHOT.jar'
    into 'dist'
    }
  
#### Execute:
   
    % ./gradlew copyJarFile  

### A task to delete files 

The file to be deleted is located at src/main/resources/static/built. The requirement is that this task should be executed automatically by gradle before the task clean.
In the build.gradle file, create a task and below inform that it will be executed after the task clean:

    task deleteFiles(type: Delete){
    delete 'src/main/resources/static/built/'
    }
    clean.dependsOn 'deleteFiles'

Then, just run the task clean and check if the files have been deleted.

    % ./gradlew clean 

Finally, make sure everything is functional and commit and merge with the master.

## 2. Analysis of an Alternative


There are several alternative java build tools to Gradle, but the one I chose to make a comparison was **Bazel.**

- **About Bazel**

Bazel was released in March 2015 by Google. Bazel is part of the open source code of Blaze tool.
Several companies use Bazel as Google, Asana, Square and others.

Bazel operates off of two configuration files: *BUILD* and *WORKSPACE*.
The presence of a *BUILD* file tells Bazel that it is looking at a package of code — this package of code includes the current directory and any subdirectories within it, unless the subdirectory contains a build file.
A typical *BUILD* will have two things: Rules and Files. A Rule is similar to a function in that it can take in Files as an input and use that input to produce some new Files as an output.
The *WORKSPACE* file is written in the BUILD language, and, like BUILD files, there can only be one *WORKSPACE* in a package. The purpose of the *WORKSPACE* file is to track the external dependencies of a project.

Another feature of Bazel is that it has a multiplatform support, that is, the same tool and the same BUILD files can be used to build software for different architectures, and even different platforms.


- **Main differences and similarities between Gradle and Bazel**

Below are some features that highlight the differences between Gradle and Bazel:

|           | Gradle    | Bazel     |
|:-----------:|:-----------:|:-----------:|
| Java Build tool |     ✔      |   ✔        |
| Open source | ✔ |  ✔ |
| Easy to use | ✔ | |
| Support Spring Boot | ✔ | |
| IntelliJ Support | ✔ | ✔ |
| Popularity | ✔ | |
| Builds and Tests | ✔ | |
| Dependency management | ✔ | ✔ |
| Object oriented | ✔ | | 

In summary, the data and analysis indicates clearly that Gradle is a better choice than Bazel for most JVM projects.
At the same time, recognizing that individual tools have unique strengths in addressing the needs and requirements of specific developer ecosystems and specific use cases.

## 3. Implementation

To implement Bazel, I first followed the paths indicated in the official documentation found at this link: https://docs.bazel.build/versions/master/install-windows.html

To build or test a project with Bazel, you typically do the following:

- Download and install Bazel;

- Set up a project workspace, which is a directory where Bazel looks for build inputs and BUILD files, and where it stores build outputs;

- Run Bazel.

Unfortunately, I couldn't implement this project, I couldn't find enough material to help me understand what needed to be done. 
What I did was part of a tutorial on the Bazel website and I managed to go to the part of making the build.
To do the build I used the following command
    
    bazel build //:<targetName (in the build file)>

In the target label, the // part is the location of the BUILD file relative to the root of the workspace.
The result of this build was:

![](./images/build%20javaTut.png "Build with Bazel")

The build created some new folders like bazel-bin and bazel-out for example:

![](./images/projStruct.png "IDE")

Fonts: 
https://blog.gradle.org/gradle-vs-bazel-jvm
https://stackshare.io/stackups/bazel-vs-gradle
https://medium.com/@shea.m.hawkins/differences-between-bazel-and-gradle-builds-c39b32c3348d


