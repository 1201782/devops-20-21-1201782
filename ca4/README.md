# Class Assignment 4 Report

## 1. Implementation

In this class assignment we work with Dockers and use the previous project(demo-vagrant) as a basis to create two containers, the 'web' and the 'db'.

To start this project you need to clone the project: https://bitbucket.org/atb/docker-compose-spring-tut-demo/src/master/ to the ca4 folder.

Then I went to the *web* folder and the **Dockerfile** and redirect the git clone for my repository: 

    git clone https: //1201782@bitbucket.org/1201782/devops-20-21-1201782.git

I also change the path to my application: devops-20-21-1201782/ca4/demo in the line below. Just as we did in the Vagrantfile on the previous project, it's necessary to change the name of the file build/libs/demo-0.0.1-SNAPSHOT.war (from 'basic' to 'demo').

### 1.1 Building

After that, you need to go to the command line and in the folder where you have the file docker-compose.yml execute:

    docker-compose build

In my case, I had some problems making the build because It didn't recognize the ./gradlew clean build command. To resolve this I inserted the line **RUN chmod u+x gradlew** to give permission to gradlew for execution and after that it worked.

To start the containers, you must also execute:

    docker-compose up

With this, the containers will be created and can now be viewed on the Docker desktop.

![](./images/docker.png )


### 1.2 Frontend

To see the frontend of the application just go to the browser using the following urls:

**web**

http://localhost:8080/demo-0.0.1-SNAPSHOT/

**h2**

http://localhost:8080/demo-0.0.1-SNAPSHOT/h2-console

To connect use the url: *jdbc: h2:tcp://192.168.33.11:9092/./jpadb*

![](./images/h2.png)

### 1.4 Publishing the images

To publish the images on the Docker hub, you must access the website https://hub.docker.com/ create an account based on the image ID of a container after that, create a repository.
Then you need to log in using the command line:

    docker login -u <user_name>

Then it will ask for the password and just put the one you registered through the website.

Now You need the docker tag command to associate the image with your Dockerhub account by including a namespace before the image name. Then do the command:

    docker tag IMAGE_ID YOUR_DOCKER_ID/repositoryname:tagname

Push:

    docker push yourdockerID/repositoryname:tagname

Once published, if you click to see the content, you will see the image below:

![](./images/db-dockerhub.png)

Do the same procedure for the other image and don't forget to rename the tag. The image that should appear is like the one below:

![](./images/web-dockerhub.png)

Below we can confirm that the two images have been published successfully.

![](./images/dockerhub.png)

### 1.5 Data base backup

To perform the task of copying the volume from the db container to the data folder, it was necessary to first execute the command below so that the container in question can use the bash language:

    docker exec -it <containerID> /bin/bash

To see the files type **ls -l**. Then you can see two files, .jar and mv.db. You need to copy de mv.db file.

In this way we found out that the path to the .mv.db file was /usr/src/app and then, just do the exit command to exit.


Then we use the command to copy the file:

    docker cp ca4_db_1:/usr/src/app/jpadb.mv.db .\data

To confirm that the file was actually copied, just type:

![](./images/copydb.png)

Here we conclude this first part.

## 2. Alternative Solution

### 2.1 Kubernetes vs. Docker

“Kubernetes vs. Docker ” is a somewhat misleading phrase, because Docker and Kubernetes are not direct competitors. Docker is a container platform and Kubernetes is a container orchestrator for container platforms like Docker.

Docker is currently the most popular container platform. When thinking about Docker you have to take into account that we have two parts that integrate it: the Docker Engine, which is the runtime that allows you to run the containers, and the Docker file, which has the necessary configurations that allow you to build the Docker image, which is the static component, and so the Docker Engine executes that image and builds the container.

Docker has a service called Docker Hub, where you can store and share images, allowing you to use a project already set up in case you don't want to start from scratch.

Kubernetes emerged to supply a container management problem, which allowed scaling, running and monitoring applications on a large scale. In addition to Kubernetes, Mesos and Docker Swarm are some of the most popular options for providing an abstraction to make a cluster of machines behave like a large machine, which is vital in a large-scale environment.

A container manager needs, among other things, to handle a large volume of containers and users interacting with each other at the same time, it also coordinates the operation of the container, facilitates the handling of authentication and security at the infrastructure level and makes load balancing. to balance the server and allow the decongestion of system resources.

Kubernetes is a competitor to Docker Swarm, as I mentioned earlier, Swarm is Docker's native cluster solution for Docker containers, which has the advantage of being fully integrated into the Docker ecosystem and uses its own API.

Kubernetes is the container orchestrator developed at Google, donated to CNCF and is now open source. It has the advantage of leveraging Google's years of experience in container management. It is a comprehensive system for automating the deployment, scheduling and scaling of applications in containers and supports many container tools, such as Docker.

For now, Kubernetes is the market leader and the standard means of orchestrating containers and deploying distributed applications. One of the great advantages of Kubernetes can be said to be: it can run on a public or local cloud service, it is highly modular and has open source.

### 2.2 Kubernetes architecture and components

Kubernetes has several components that communicate through the API server, and each has its own function. You can divide them into Control Plane, Nodes and Pods.

![](./images/kubernetsComponents.png)
*Font: https://www.sumologic.com/blog/kubernetes-vs-docker/*

The Control Plane is the orchestrator and within it there are several other components that facilitate the orchestration of Kubernetes.

The Nodes is where the containers are deployed, the nodes represent the physical infrastructure on which the application is run.

Pods are the low-level resources within the Kubernetes cluster. A Pod can have one or more containers but usually only has one. When creating a cluster it is necessary to define the resources that can be used by the pods, such as memory for example.

### 2.3 Kubernetes and Docker

Although Kubernetes can use other container sources and runtimes, it was designed to work well with Docker, and much of the Kubernetes documentation was written with Docker in mind. The most basic use case for Kubernetes is Kubernetes + Docker, and Kubernetes includes Docker-centric tools such as Kompose, which converts Docker Compose commands and settings so they can be used by Kubernetes. I followed the tutorial on kubernetes.io.

In turn, Docker adopted Kubernetes and, in fact, offered its own integrated Kubernetes distribution. The sale of Docker Enterprise to Mirantis in late 2019 (along with Docker's own renewed focus on developer-oriented tools) further emphasized Docker's confidence in Kubernetes and other container infrastructure providers. This is emphasized by Mirantis' stated intention to eliminate Docker Swarm and establish Kubernetes as the standard orchestration tool for Docker Enterprise.

The conclusion reached is that Kubernetes and Docker are industry standards in their respective main areas of expertise and, together, provide a well-integrated platform for managing, deploying and orchestrating containers at scale. It was never really a question of Kubernetes versus Docker, but Kubernetes and Docker.

### 2.4 Alternative implementation

In this part we will use the MiniKube which is a tool that makes it easy to run Kubernetes locally, running a cluster of Kubernetes with a single node.

First we need to install de MiniKube. I used the Windows Installer to do that. After that if you write on the command line **minikube version**, you can verify if it was installed. Then you can write **minikube start** to star a cluster.

To verify Nodes, write **kubectl get nodes**.

![](./images/minikubeNode.png)

Now we need do deploy the app on Kubernetes. The command that I used is below:

    kubectl create deployment db1.0 --image=be50f30bbb91/devops-20-21-1201782:db1.0
    kubectl create deployment web1.0 --image=be50f30bbb91/devops-20-21-1201782:web1.0

![](./images/kubernetesrunning.png)

I used the images published on Docker Hub.

Now you need to start the application. For this we use Kompose, which is a conversion tool from Docker Compose to Kubernetes. First it was necessary to download the tool. Next, I created a folder and pasted the .exe file there, along with the docker-compose.yml file. And then we go to the command line, and we go to the path of the folder that was created, there we execute:
    
    kompose-windows-amd64.exe convert --volumes hostPath

With that, some files will be generated, as we can see below and we will need to make some changes to them.

![](./images/kompose.png)

In the **deployment.yaml** files where we have *-image: " "* we will put the address of our image built in the Docker hub for the following web and db VMs.
The **services.yaml** file added the line *type: NodePort* below the *'spec'*. This is necessary so that we can access the Service from outside the cluster.

In the **default-networkpolicy.yaml** file, in the first line, we will replace apiVersion with *apiVersion: networking.k8s.io/v1* this will serve to configure Kubernetes in our project.
Returning to the command line to run the Kubernetes we will do:

    minikube start

![](./images/kubestart.png)

After successfully starting, we connect to our Docker hub account using the command

    docker login

Next, we will apply the new settings using the command:

    kubectl apply -f.

And so, to check our pods just type:

    kubectl get pod -o wide


![](./images/kubegetPod.png)

We will now run our Web VM using the command:

    kubectl exec -ti web-86547cbbdc-p5r7l - bash

![](./images/kubebuild.png)

Then you need to put the copy of the war file there. And then we can exit. Then it is necessary to check what is the IP of our virtual machine and then we execute the command:

    minikube service web

![](./images/kubecp.png)

Now everything is ready and to check it just go to the browser. To see the frontend, we put the IP that was created and the name that we gave.

![](./images/kubeweb.png)

Finally, to see the database, use the same url and add / h2-console. In the Access url place the IP generated by the VM of your database.

![](./images/kubedb.png)

So we are done with one more task.





