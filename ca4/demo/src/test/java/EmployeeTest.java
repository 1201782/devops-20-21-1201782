import com.greglturnquist.payroll.Employee;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    @Test
    void getFirstName() {
        Employee employee = new Employee("Maria", "Ramos", "Chef", "marramos@gmail.com");
        String expected = "Maria";
        String result = employee.getFirstName();
        assertEquals(result, expected);
    }
    @Test
    void getEmail() {
        Employee employee = new Employee("Maria", "Ramos", "Chef", "marramos@gmail.com");
        String expected = "marramos@gmail.com";
        String result = employee.getEmail();
        assertEquals(result, expected);
    }

    @Test
    void getFirstNameFailure() {
        Employee employee = new Employee("Maria", "Ramos", "Chef", "marramos@gmail.com");
        String expected = "Joana";
        String result = employee.getFirstName();
        assertNotEquals(result, expected);
    }

    @Test
    void firstNameNull() {
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee(null, "Ramos", "Chef", "marramos@gmail.com");
        });
    }

    @Test
    void firstNameEmpty() {
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("", "Ramos", "Chef", "marramos@gmail.com");
        });
    }

    @Test
    void lastNameNull() {
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Maria", null, "Chef", "marramos@gmail.com");
        });
    }

    @Test
    void lastNameEmpty() {
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Maria", "", "Chef", "marramos@gmail.com");
        });
    }

    @Test
    void descriptionNull() {
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Maria", "Ramos", null, "marramos@gmail.com");
        });
    }

    @Test
    void descriptionEmpty() {
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Maria", "Ramos", "", "marramos@gmail.com");
        });
    }

    @Test
    void emailEmpty() {
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Maria", "Ramos", "Chef", "");
        });
    }

    @Test
    void emailNull() {
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Maria", "Ramos", "Chef", null);
        });
    }

    @Test
    void emailInvalid() {
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Maria", "Ramos", "Chef", "maria.com");
        });
    }

    @Test
    void emailInvalid2() {
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Maria", "Ramos", "Chef", "maria@");
        });
    }
}