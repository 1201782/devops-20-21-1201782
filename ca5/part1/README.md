# Class Assignment 5 Report 1

## 1.Implementation

In this first part we learn how to create a pipeline in Jenkins. For this we use the project 'gradle basic demo part 1' made earlier.

### 1.1 Installing Jenkins

Previously we downloaded Jenkins via a .war file and ran it using the command: **java -jar jenkins.war**

Then we go to localhost:8080 we create our login and so we are ready to create our first pipeline.

### 1.2 Jenkinsfile

The first things that I did was create the ca5 and part1 folders, and clone the ca2/part1 project there. 
Then I created the Jenkinsfile and built the project using the command './gradlew build', thus creating a folder called build.

![](./images/jenkinsfile.png)

The creation of this file is the great challenge. Although simple, depending on how it is built it is necessary to make some changes as was my case.

What I did first was to create the pipeline structure as requested, that is, create the Checkout part, the Assembling, the Tests and the Archiving.

My operating system is Windows, so I need to use the bat command and not sh as in unix. And when you need to run gradlew you need to use the command 'call [path of the file gradlew.bat] -p [name of the file where you want to run] [command gradlew'.

Then, as I chose not to fork my repository, I had to go all the way until I found the right folder. It took some time to discover this.

For the tests, I downloaded Junit from Jenkins and the framework for this I found on the internet and in the support slides.

Then I just needed to archive using the archiveArtifacts command, as you can see in the jenkinsfile image above.

### 1.3 Creating the pipeline

After structuring the Jenkinsfile, I went to my Jenkins page and created a new job. I named it 'PipeCa5' and selected Pipeline. Then I copied the contents of the Jenkinsfile into the scrip pipeline, ok and then I build it.
To follow the process I downloaded the Open Blue Ocean plugin, which helped to see exactly what errors were occurring.

So the pipeline was built as we can see in the image below.

![](./images/pipeline.png)

So I ended this first part.