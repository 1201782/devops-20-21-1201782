# Class Assignment 5 Report - Part 2

## 1. Implementation

### 1.1 Building

The first thing I did was clone the ca2/part2 project to ca5/part2 and build it.
Here I had some problems with installing the frontend, which had to do with npm. Because I have a space in my UserName, I created a new User with a '~1', and I couldn't build it.
After searching, I found that if I typed in the command line:

    npm config set cache "C:\Users\Thayane Marcelino\AppData\Roaming\npm-cache" --global

The system already realized that it should not create a different folder, but use the existing one. That way it worked.

After building I added Jenkinsfile and Dockerfile.

### 1.2. Jenkinsfile

The Pipeline final configuration can be seen below, but to get there, several researches were carried out.

![](./images/pipeline.png)

To make the Javadoc part, I first downloaded the Javadoc Plugin and restarted Jenkins.
Then I researched how I could publish to Jenkins as requested, and then what I did was go to the menu and go to Pipeline Syntax (in my case I opened another pipeline that I already had to find this menu).
As you can see below I chose PublishHTML as Sample Step, then I put the path to the javadoc file and did the generate Pipeline Script, then the syntax was generated which I pasted into the Jenkinsfile.

![](./images/syntaxpipeline.png)

As you can see below I chose PublishHTML as Sample Step, then I put the path to the javadoc file and did the generate Pipeline Script, then the syntax was generated which I pasted into the Jenkinsfile.
As we can see on the Stage ('Javadoc') I first ask gradlew to run the javadoc, so the docs file is generated inside the build file. 
And the line below is responsible for publishing on Jenkins, after building the pipeline it will look like the image below. 

![](./images/javadocmenu.png)
--
![](./images/javadoc.png)

Next I needed to add the Dockerfile to the project, so I could build an image with Tomcat.
The basic settings were: install git, node and npm. Then I add the .war file to the tomcat path.

The plugin needed to build and use Docker from pipelines is Docker Pipeline, so I downloaded it.
In regard to building and publishing the image, I first created a credential on Jenkins to access my docker hub repository. 
Then, according to the syntax researched, in the first line I built the image and then published it using the docker hub credential I created earlier.
Basically what I did was tell Jenkins to build an image called "app" based on the *docker.build* statement, which creates the image.
The *docker.buil* accepts the repository in the format "username/repository", which is in the Dockerhub, and what I did next was to indicate the path to the Dockerfile because at the beginning of this stage it is in the pipeline workspace.

![](./images/docker.png)

With that, I just needed to create a new pipeline job, pointing to the location where the Jenkinsfile was in my repository, and then build.
After making the build successfully if I clicked on the status I could see the image below.

![](./images/build%20jenkins.png)

So, this part is concluded.

## 2. Alternative Analysis

![](./images/buddylogo.png)

As an alternative to Jenkins I chose to research about Buddy. Buddy is a Polish Company founded in 2015. Buddy is a web-based CI/CD pipeline tool that can be used to create, test and deploy websites and applications with code from GitHub, Bitbucket and GitLab.

It is available in public cloud and on-premises variants. cloud and on-premises solutions include Docker layer caching, pipelines and concurrent steps, parallel build and testing, repository and artifact caching, and vCPU and RAM scaling. 

Along with the integration with Docker and Kubernetes, it is possible to use it with Blockchain technology. All builds and commands in Buddy run in isolated Docker containers. Buddy also supports integration with popular cloud providers like AWS, Digital Ocean, Kubernetes, Microsoft Azure, Rackspace and others.

Setting up the pipeline in Buddy is easy as the configuration can be done through the GUI. There is a feature to instantly export the pipeline configuration to a YAML file (to use the pipeline as code).

One disadvantage is that if you want a more complete version you have to pay and the starting amounts are $75 per month per team or $35 per month per user.

Below I've selected the main characteristics common or not between Jenkins and Buddy.

|  Buddy    | Jenkins |
|:-----------:|:-----------:|
|  ✔ Partially free |  ✔ 100% Free |     
| ✔ Best for small and medium businesses  | ✔  Best for small, medium and large companies|  
| ✔ Access Controls/Permissions | ✔ Access Controls/Permissions|  
| ✔ Build Log | ❌  |  
| ✔ Configuration Management | ❌ |  
| ✔ Continuous Delivery | ✔ Continuous Delivery |  
| ✔ Continuous Deployment | ✔ Continuous Deployment  |  
| ✔ Debugging | ❌   |  
| ✔ Quality Assurance | ❌   |  
| ✔ Testing Management | ✔ Testing Management |  
| ❌ | ✔ Deployment in Linus|
| ✔ Deployment in Android | ❌ |
| ✔ It's not necessary install | ❌ Need install|

As we can see from the features compared above, we notice that Buddy is the tool that brings the most advantages. 
Of course, depending on the type of company and job Jenkins will be the best option, however overall Buddy is better.

## 3. Alternative Implementation

To see if Buddy is better, below I'll show you how I created the same pipeline I created in Jenkins.

Steps to create the Buddy pipeline:

1- To start, I first went to the buddy website(https://buddy.works/);

2- Then, I chose the free option and associated my Bitbucket account;

3- Then I linked the project to my Bitbucket devops repository;

4- I clicked on create pipeline;

5- In the pipeline, I went to Actions and selected the Gradle container. In Run, I put the following tasks:
cd ca5/part2
gradle assemble
grade test
javadoc gradle
In Environment, I selected the 6.8.3 version of the gradle and in Actions I changed the step name.

6- Added here a new stage to make the Build Docker image. In Setup I put the path to the Dockerfile(ca5/part2/Dockerfile), in Options I linked my repository to the Docker hub (before I did an integration in the left side menu, which is like the credential we made in Jenkins);

7- So I saved everything and ran the pipeline.

![](./images/buddypipeline.png)

As you can see in the image above everything went well, but I had some problems along the way. The first error was that the 'compileJava', was running Java 8 and my project was with Java 11, to fix it I switched to Java 8 named project. Then what I was doing wrong is that I hadn't associated my Docker container with my Docker Hub repository. And third, I was using the gradle clean test to run the tests and it was preventing the project from doing assemble.

If we go to the Docker hub we will be able to see the published images as expected.

![](./images/dockerhub.png)

My opnion about Buddy is that it is very visual and relatively easy to use. However, I found some difficult to find more consistent tutorials and materials about the tool. 
I think Jenkins worked better for me, but that's just my opnion right now.

So, this last part is concluded.