# Class Assignment 5 Report - Part 2

## 1. Implementation

In this second part the objective is to build a pipeline a little more complex than the first part. 

In addition to all the steps from the previous part, we have to generate javadoc and publish to Jenkins, then generate a docker image with Tomcat and war file and publish to Docker hub.

### 1.1 Building

The first thing I did was clone the ca2/part2 project to ca5/part2 and build it.
Here I had some problems with installing the frontend, which had to do with npm. Because I have a space in my UserName, I created a new User with a '~1', and I couldn't build it.
After searching, I found that if I typed in the command line:

    npm config set cache "C:\Users\Thayane Marcelino\AppData\Roaming\npm-cache" --global

The system already realized that it should not create a different folder, but use the existing one. That way it worked.

After building I added Jenkinsfile and Dockerfile.

### 1.2. Jenkinsfile

The final configuration can be seen below, but to get there, several researches were carried out.
To make the Javadoc part, I first downloaded the Javadoc Plugin and restarted Jenkins. 
Then I researched how I could publish to Jenkins as requested, and then what I did was go to the menu and go to Pipeline Syntax (in my case I opened another pipeline that I already had to find this menu).
As you can see below I chose PublishHTML as Sample Step, then I put the path to the javadoc file and did the generate Pipeline Script, then the syntax was generated which I pasted into the Jenkinsfile.
![](./images/syntax)


## 2. Analysis of the Alternative


## 3. Alternative Implementation
