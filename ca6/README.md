# Project Personal Finance - DevOps

 *Track 4 - Gradle; local smoke test of App with Docker containers; publish docker images; manually execute App in local containers*

- **Maven to Gradle**

The first thing we needed to do was fork our project. Then in my case as I needed Gradle to build the build I needed to understand some things.

One way to do this is to transform the pom.xml file to build.gradle. However, I read that it could cause a series of problems such as dependencies, tests and others. So I chose to use gradle at the same time as Maven, which at first won't generate big conflicts. 

For that we went to Spring Initializr and chose to generate a Gradle project, using Java 1.8 and with Packaging war. We also insert dependencies that we use in our project, like H2, Spring web, hateoas and others.

![](./images/spring.png)

After generating the gradle configuration files, just add the generated files to our project.

- **Smoke Test with Docker**

To do the smoke test, which is a test if our backend and frontend are working correctly, I first created the docker-compose.yml file. This file will orchestrate the creation of the containers.

![](./images/docker-compose.png)

Afterwards, I created two folders, one with the frontend Dockerfile and one with the database. The Dockerfile db, I used the same as the ca4 project, and the Dockerfile web I also reused, but I needed to change some things. I changed the repository path, then the working directory I would use and then I changed the name of my war file as you can see in the image below.

![](./images/web_dockerfile.png)

So I went to the command line and ran **docker-compose build** in the path where I had my docker-compose file. So, the images were created, but to put the containers to work I did the following **docker-compose up**.

Thus, was generated both web and db containers.

![](./images/docker.png)

Then I went to the browser to try to see my application, but it didn't work. This is because, as my colleagues needed to make changes to the application.properties, some differences arose. But they were easily detected and changed.
In the Application.properties file, I first checked if my database configuration was correct, using localhost:9092 (as configured in docker-compose.yml). Then, I checked if the server was going the right way (/switch_2020_g4_devops-0.0.1-SNAPSHOT) and finally I commented on what was causing conflict, which in this case was the datasource line.

![](./images/appProp.png)

After that the frontend still didn't work and the database, obviously, didn't have the data, because I was forgetting to run gradle in the web container. 
Knowing this, I accessed my web container using the **docker-compose exec web bash** command, and then did **./gradlew clean build**. 
After doing the build I copied the war file to /usr/local/tomcat/webapps/. And so it worked.

To access the **db** I used: http://localhost:8082/login.do?jsessionid=0352717b74233936fc24ac423cad49cd

To access the **web* I used: http://localhost:8080/switch_2020_g4_devops-0.0.1-SNAPSHOT/api/allcategories

As we can see below, the database shows the data of our application correctly and the front end when trying to access the categories also shows the data as expected.

![](./images/db.png)

-----------------------

![](./images/frontend.png)

- **Pipeline**

As requested, we had to create a pipeline in Jenkins with the following stages: assemble the application; generate and publish
javadoc in Jenkins; run tests (unit and integration) and publish its results in Jenkins, including code coverage; 
publish the distributable artifacts in Jenkins and build 2 docker images (app + db) and publish them in docker hub.

For that I created the Jenkinsfile file with the following settings:

![](./images/jenkinsfile1.png)
![](./images/jenkinsfile2.png)

To access Jenkins I needed to direct to another port, as the 8080 was already being used. For this I used the command **java -jar jenkins.war --httpPort=9090**

So as in ca5, I added a new job, created a pipeline that will fetch the Jenkinsfile from the bitbucket repository and then build the pipeline.

As you can see below when building the pipeline everything went well. 

Using Open Blue Ocean:
![](./images/pipeline.png)

-----------------------
Using default Jenkins:
![](./images/pipeline2.png)

What we can see is that in this pipeline two containers were created and both were published in the docker-hub.

![](./images/dockerhub.png)

- **Conclusion**

This track was relatively easy compared to the others from my point of view. I could have used virtual machines using Vagrantfile (I got to create them as you can see in the repository), but I didn't think it would be worth it, as Docker does the same job, so I chose not to use them.

Anyway, I enjoyed doing this work because it reinforced the knowledge I acquired throughout this chair and I hope to use them again soon. 

Thanks for everything teacher.